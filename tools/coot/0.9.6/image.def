BootStrap: docker
From: scientificlinux/sl:7

%environment
    export LC_ALL=C
    export PATH=/opt/coot/bin:$PATH
    export LD_LIBRARY_PATH=/opt/coot/lib

%post

    # https://strucbio.biologie.uni-konstanz.de/ccp4wiki/index.php?title=Coot#Installing_Coot_on_Linux

    # utilities
    yum -q install -y wget gunzip tar

    # coot system dependencies
    yum -q install -y mesa-libGLU-9.0.0 libICE-1.0.9 libSM-1.2.2 libXmu-1.1.2 libXmu-1.1.2 libXt-1.1.5 gtk2-2.24.31 libgomp-4.8.5

    # mesa ugly fix 
    yum -q install -y mesa-dri-drivers mesa-libGL

    # tclsh ugly fix
    yum -q install -y expectk

    # load svg icon ugly fix
    yum -q install -y gdk-pixbuf2 librsvg2

    # https://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot/binaries/release/
    cd /opt
    wget -q https://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot/binaries/release/coot-0.9.6-binary-Linux-x86_64-scientific-linux-7.6-python-gtk2.tar.gz
    gunzip coot-0.9.6-binary-Linux-x86_64-scientific-linux-7.6-python-gtk2.tar.gz 
    tar -xf coot-0.9.6-binary-Linux-x86_64-scientific-linux-7.6-python-gtk2.tar
    mv coot-Linux-x86_64-scientific-linux-7.6-gtk2-python coot
    rm -f coot-0.9.6-binary-Linux-x86_64-scientific-linux-7.6-python-gtk2.tar

    # ugly packaging upstream
    ln -s librfftw.so.2 coot/lib/libsrfftw.so.2 
    ln -s libfftw.so coot/lib/libsfftw.so.2 

%runscript
    exec "$@"

%labels
    Author IFB

%test
    export LC_ALL=C
    export PATH=/opt/coot/bin:$PATH
    export LD_LIBRARY_PATH=/opt/coot/lib
    ls -lrt /opt/coot/lib
    # coot --version
