BootStrap: docker
From: nvidia/cuda:10.2-devel-ubuntu18.04

%labels
    Author IGBMC

%files
    /src/ccp4-ccpem-modeller/7.1.004-1.5.0-9.25/ccp4-7.1.004-shelx-arpwarp-linux64.tar.gz /opt
    /src/ccp4-ccpem-modeller/7.1.004-1.5.0-9.25/modeller.key /opt


%post
    apt-get -y -qqqq update
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
    apt-get -y -qqqq install bsdtar\
    csh\
    curl\
    ffmpeg\
    grace\
    imagemagick\
    libcgi-pm-perl\
    libfontconfig1-dev\
    libgl1-mesa-dev\
    libglu1-mesa-dev\
    libncurses5\
    libsm6\
    libxcursor1\
    libxcomposite1\
    libxdamage1\
    libxext-dev\
    libxmu6\
    libxrandr2\
    libxrender-dev\
    libxt6\
    libxxf86vm1\
    mesa-utils\
    openbabel\
    openjdk-8-jre\
    php-cli\
    povray\
    pymol\
    tcsh\
    texlive-latex-extra\
    wget

    # CCP4
    cd /opt
    bsdtar xzf ccp4-7.1.004-shelx-arpwarp-linux64.tar.gz
    touch $HOME/.agree2ccp4v6
    cd /opt/ccp4-7.1
    ./BINARY.setup > /dev/null
    rm /opt/ccp4-*-shelx-arpwarp-linux64.tar.gz

    # load ccp4 environment
    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    # modeller
    cd /opt
    wget https://salilab.org/modeller/9.25/modeller_9.25-1_amd64.deb
    env KEY_MODELLER=$(cat /opt/modeller.key) dpkg -i modeller_9.25-1_amd64.deb
    rm /opt/modeller_9.25-1_amd64.deb
    rm /opt/modeller.key

    # ccpem
    cd /opt
    wget https://www.ccpem.ac.uk/downloads/ccpem_distributions/ccpem-1.5.0-linux-x86_64.tar.gz
    tar -xzvf ccpem-1.5.0-linux-x86_64.tar.gz > /dev/null
    touch $HOME/.agree2ccpemv1
    cd ccpem-1.5.0
    ./install_ccpem.sh
    rm /opt/ccpem-1.5.0-linux-x86_64.tar.gz

%environment
    export LC_ALL=C

    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    . /opt/ccpem-1.5.0/setup_ccpem.sh

%test
    export LC_ALL=C

    # check ccp4
    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash
    ccp4-python --version 2>&1 | grep "Python 2.7"

    # check ccpem
    . /opt/ccpem-1.5.0/setup_ccpem.sh
    ccpem-python --version 2>&1 | grep "Python 2.7"
    relion --version 2>&1 | grep "RELION version: 3.1.1"
